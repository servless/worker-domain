# 域名停靠

基于 `CloudFlare Workers`。

- https://domain.ixxx.workers.dev/ （域名列表页面）
- https://domain.ixxx.workers.dev/text （域名 + 价格）

## 部署教程

### 通过 CI 发布至 CloudFlare

1. 从 CloudFlare 获取 `CLOUDFLARE_API_TOKEN` 值，并设置到项目。

   - `https://github.com/<ORG>/<REPO>/settings/secrets/actions`
   - `https://<GITLAB_DOMAIN>/<ORG>/<REPO>/-/settings/ci_cd` (变量)

2. **可选**）设置`别名`。创建 `KV`、，并绑定到此 Workers 服务。
   - 2.1a 手动后台绑定，（`Settings` -> `Variables` -> `KV Namespace Bindings` -> `Add binding` -> `Variable name (data)`, `选择创建的 KV`）
   - 2.1b 通过命令行创建。按照**本地部署**的第 6 步，创建和保存 `KV`

### 本地部署到 CloudFlare

1. 注册 [CloudFlare 账号](https://www.cloudflare.com/)，并且设置 **Workers** 域名 (比如：`xxx.workers.dev`)

2. 安装 [Wrangler 命令行工具](https://developers.cloudflare.com/workers/wrangler/)。
   ```bash
    npm install -g wrangler
   ```
3. 登录 `Wrangler`（可能需要扶梯）：

   ```bash
   # 若登录不成功，可能需要使用代理。
   wrangler login

	 # 或者直接设置环境变量 CLOUDFLARE_API_TOKEN
	 export CLOUDFLARE_API_TOKEN="xxxx"
   ```

4. 拉取本项目,并进入该项目目录：

   ```bash
   git clone https://framagit.org/servless/worker-domain.git
   cd worker-domain
   ```

5. 修改 `wrangler.toml` 文件中的 `name`（proj）为服务名 `xxx`（访问域名为：`proj.xxx.workers.dev`）

6. 创建 **Workers** 和 **KV**，并绑定 `KV` 到 `Workers`

   1. **创建 KV，并设置 domain 值**

      1. 创建名为 `data` 的 `namespace`

         ```bash
            wrangler kv:namespace create data
         ```

         得到

         ```bash
            ⛅️ wrangler 2.15.1
            --------------------
            🌀 Creating namespace with title "domain-data"
            ✨ Success!
            Add the following to your configuration file in your kv_namespaces array:
            { binding = "data", id = "c63f7dad63014a70847d96b900a4fc3f" }
         ```

         将上述命令得到的 `kv_namespaces` 保存到 `wrangler.toml` 中，即

         ```bash
            # 替换当前项目该文件内相关的数据，即只需要将 id 的值替换为上一步骤得到的值
            kv_namespaces = [
            { binding = "data", id = "c63f7dad63014a70847d96b900a4fc3f" }
            ]
         ```

   2. 将 `data` 值保存到 `KV namespace`

      ```bash
         wrangler kv:key put --binding=data 'domains' '["domain":"x.com", "regdate":"", "expdate":""]'
      ```

7. 发布

   ```bash
    wrangler deploy
   ```

   发布成功将会显示对应的网址

   ```bash
    Proxy environment variables detected. We'll use your proxy for fetch requests.
   ⛅️ wrangler 2.13.0
        --------------------
        Total Upload: 0.66 KiB / gzip: 0.35 KiB
        Uploaded proj (1.38 sec)
        Published proj (4.55 sec)
                https://proj.xxx.workers.dev
        Current Deployment ID:  xxxx.xxxx.xxxx.xxxx
   ```

## 仓库镜像

- https://git.jetsung.com/servless/worker-domain
- https://framagit.org/servless/worker-domain

export default {
  // 定义 indexPage 方法
  indexPage(domains: string, domain: string, env: any): string {
		let domainContent = '';

		try {
			const domainList: any[] = JSON.parse(domains);
			domainContent = domainList.map(
                        (value) => `
                          <tr>
                            <td class="domain">${value['domain']}</td>
                            <td>${value['price']}</td>
                            <td>${value['regdate']}</td>
                            <td>${value['expdate']}</td>
                          </tr>
                        `
                      )
                      .join('')
		} catch (error: any) {
			console.error(error.message);
		}

		let statistics_content = '';

		if (env.TJ_51LA) {
			statistics_content = `
					<script charset="UTF-8" id="LA_COLLECT" src="//sdk.51.la/js-sdk-pro.min.js"></script>
					<script>LA.init({id:"${env.TJ_51LA}",ck:"${env.TJ_51LA}"})</script>
					<script id="LA-DATA-WIDGET" crossorigin="anonymous" charset="UTF-8" src="//v6-widget.51.la/v6/${env.TJ_51LA}/quote.js?theme=0&f=12&display=0,0,1,1,1,1,1,1"></script>
				`;
		}

    return `
      <html lang="zh-CN">
        <head>
          <title>域名出售 | Domain Sale</title>
					<meta charset="UTF-8">
          <style>
            body {
              text-align: center;
              margin: 0 auto;
            }
						.sale {
							margin-top: 40px;
						}
            table {
              border-collapse: collapse;
              width: 50%;
              margin: auto;
            }
            table, th, td {
              border: 1px solid black;
            }
            .domain {
              text-align: center;
              font-weight: bold;
            }
            .contact {
              font-size: 25px;
              color: #910000;
            }
							.price-cny {
								font-weight: bold;
								color: #CC0000;
							}
						footer {
							margin-top: 40px;
						}
          </style>
        </head>
        <body>
          <div class='sale'>
            <h3>域名出售 | Domain Sale</h3>
              <p>
                <table>
                  <thead>
                    <tr>
                      <td class="domain">域名<br>Domain</td>
                      <td>价格<span class="price-cny">（CNY）</span><br>Price</td>
                      <td>注册时间<br>Registration time</td>
                      <td>过期时间<br>Expiration time</td>
                    </tr>
                  </thead>
                  <tbody>
                    ${domainContent}
                  </tbody>
                </table>
              </p>
              <p class="contact">
                The domain <span class="domain">${domain}</span> is for sale. Contact <a href="mailto:flydochen@outlook.com">flydochen@outlook.com</a><br>
                域名 <span class="domain">${domain}</span> 正在出售。联系邮箱 <a href="mailto:flydochen@outlook.com">flydochen@outlook.com</a>
              </p>
          </div>
					<footer>${statistics_content}</footer>
        </body>
      </html>
    `;
  },

	textPage(domains: string): string {
		let content: string = '';

		try {
			const domainList: any[] = JSON.parse(domains);
			content = domainList.map(
				(value) => {
					let arr: Array<string> = [ value['domain'] ];
					const numMatch = value['price'].match(/\d+(?:\.\d+)?/);
					if (numMatch) {
							arr.push(parseFloat(numMatch[0]).toFixed(2));
					}
					return arr.join(',');
				}
			).join('<br/>')
		} catch (error: any) {
			console.error(error.message);
		}

		return content;
	},

  // 定义异步 fetch 方法
  async fetch(request: Request, env: any) {
    const url = new URL(request.url);

    if (url.hostname !== 'localhost' && url.hostname !== '127.0.0.1') {
      if (url.protocol !== 'https:') {
        return Response.redirect('https://' + url.hostname + url.pathname + url.search, 301);
      }
    }

		console.log(url);

    const fromDomain = url.searchParams.get('from');
    const currentDomain: string = fromDomain ?? String(request.headers.get('host'));

    const domains = await env.data.get('domains');

		let html: string;
		if (url.pathname === '/favicon.ico') {
			return new Response(null, {
				headers: {
					'content-type': 'image/x-icon',
				},
			});
		} else if (url.pathname === '/text') {
			html = this.textPage(domains);
		} else {
			const tj51la = await env.data.get('tj51la');
			html = this.indexPage(domains, currentDomain, env);
		}

    return new Response(html, {
      headers: { 'content-type': 'text/html;charset=UTF-8' },
    });
  },
};
